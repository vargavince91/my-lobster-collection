# My Lobster Collection

This is a repository for collecting articles I find interesting and want to read, explore, study and remember. I found many of the articles on [YC Hacker News](https://news.ycombinator.com/) and on [Lobsters](https://lobste.rs/), hence the name. I am experimenting with "misusing" the GitLab Issues board for organizing and tagging the articles I want to read.

My brain can't handle complexity, noise and fluff. I can't subtract the essence, the most useful and interesting ideas from the articles I read online. I need to translate these articles into bullet points, I need to use fancy markdown to highlight important words.

I rehash other people's articles. I always give credit and provide links to the original author, publisher. However, if you don't want to see your article in my repository, feel free to contact me and I'll remove your article(s) from the repo.

Avatar Source: [The Truth About Shrimp, Salmon, Lobster, Crab and More](http://magazine.foxnews.com/food-wellness/truth-about-shrimp-salmon-lobster-crab-and-more) on FoxNews
