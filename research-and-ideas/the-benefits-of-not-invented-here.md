# [The Benefits of "Not Invented Here"](http://hbswk.hbs.edu/item/the-benefits-of-not-invented-here) by Sean Silverthorne

> Q&A with professor Henry Chesbrough on his new book, Open Innovation: The New Imperative for Creating and Profiting from Technology

Innovation used to be
* managed in closed, internally focused manner,
but today, it must be
* managed as an open system, with a far greater external focus

## Examples of open innovation

* IBM invests in software such as Java and Linux in order to integrate many companies' products and services for the company's customers
* Intel invests in university research and in startups as a VC
* IBM and P&G allow others to license their ideas for their own businesses, P&G allows licensing any patented technology not in use in one of its own businesses within three years.

## Benefits of open innovation

Basically, higher growth and profits.

* Incorporating ideas into the business
* Licensing and spin-off ventures turn underused tech into additional royalties and equity
* Injecting competition into one's own innovation system

## Smaller companies and open innovation

In the U.S., small companies (less than 1,000 employees) R&D spending increased from 4 in 1981 to 22 in 1999. Great technology and ideas can be found in companies of all sizes. Put differently, there appear to be fewer economies of scale in R&D than there used to be.

### How can small companies exploit open innovation

* Recruit experienced scientists, engineers, and managers from large organizations
* Work closely with universities. University research is accessible to companies of all sizes. Small companies have less of a "not invented here" attitude, so they absorb a good university project faster.
